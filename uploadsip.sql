-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 13, 2022 at 08:01 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uploadsip`
--

-- --------------------------------------------------------

--
-- Table structure for table `file_meta_data`
--

CREATE TABLE `file_meta_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_unit` bigint(20) UNSIGNED NOT NULL,
  `nama_file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` bigint(20) NOT NULL,
  `extensi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isPrivate` tinyint(1) NOT NULL DEFAULT 1,
  `isParentPrivate` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `keterangan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expDate` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `file_meta_data`
--

INSERT INTO `file_meta_data` (`id`, `id_unit`, `nama_file`, `size`, `extensi`, `path`, `isPrivate`, `isParentPrivate`, `status`, `keterangan`, `expDate`, `created_at`, `updated_at`) VALUES
(5, 41, 'webpack.mix.js', 572, 'js', 'public/IT/Item/webpack.mix.js', 1, 0, 1, NULL, '2022-03-12', '2022-03-12 20:04:07', '2022-03-12 20:04:07'),
(6, 42, 'server.php', 584, 'php', 'public/Gizi/server.php', 0, 0, 1, NULL, '2022-03-11', '2022-03-12 20:33:23', '2022-03-12 20:33:23');

-- --------------------------------------------------------

--
-- Table structure for table `folders`
--

CREATE TABLE `folders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_unit` bigint(20) UNSIGNED NOT NULL,
  `nama_folder` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `root` tinyint(1) NOT NULL DEFAULT 0,
  `isPrivate` tinyint(1) NOT NULL DEFAULT 0,
  `isParentPrivate` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `folders`
--

INSERT INTO `folders` (`id`, `id_unit`, `nama_folder`, `path`, `root`, `isPrivate`, `isParentPrivate`, `status`, `created_at`, `updated_at`) VALUES
(1, 41, 'IT', 'public/IT', 1, 0, 0, 1, '2022-03-12 18:46:44', '2022-03-12 18:46:44'),
(2, 41, 'Item', 'public/IT/Item', 0, 0, 0, 1, '2022-03-12 18:55:40', '2022-03-12 18:55:40'),
(3, 42, 'Gizi', 'public/Gizi', 1, 0, 0, 1, '2022-03-12 20:18:11', '2022-03-12 20:18:11');

-- --------------------------------------------------------

--
-- Table structure for table `jabatans`
--

CREATE TABLE `jabatans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jabatans`
--

INSERT INTO `jabatans` (`id`, `nama_jabatan`, `created_at`, `updated_at`) VALUES
(1, 'Pimpinan', '2020-03-13 06:56:39', '2020-03-13 06:56:39'),
(2, 'Staff', '2020-03-13 06:56:39', '2020-03-13 06:56:39');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2019_03_24_050657_create_units_table', 1),
(3, '2019_03_25_020458_create_jabatans_table', 1),
(4, '2019_03_27_024125_create_file_meta_data_table', 1),
(5, '2019_03_28_083958_create_folders_table', 1),
(6, '2019_07_03_092154_create_trashes_table', 1),
(7, '2019_10_12_000000_create_users_table', 1),
(8, '2019_11_12_003609_create_user_logs_table', 1),
(9, '2019_11_18_094221_create_permissions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT 0,
  `create` tinyint(1) NOT NULL DEFAULT 0,
  `update` tinyint(1) NOT NULL DEFAULT 0,
  `delete` tinyint(1) NOT NULL DEFAULT 0,
  `download` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `id_user`, `view`, `create`, `update`, `delete`, `download`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 0, NULL, NULL),
(58, 57, 1, 1, 1, 1, 1, NULL, '2022-03-12 18:55:03'),
(59, 58, 1, 1, 1, 1, 1, '2022-03-12 20:18:40', '2022-03-12 20:20:33');

-- --------------------------------------------------------

--
-- Table structure for table `trashes`
--

CREATE TABLE `trashes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isFile` tinyint(1) NOT NULL DEFAULT 1,
  `nama_asli` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_trash` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latest_path` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `trash_path` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `expired_date` date NOT NULL,
  `isTrashRemovedPermanently` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trashes`
--

INSERT INTO `trashes` (`id`, `nama_unit`, `isFile`, `nama_asli`, `nama_trash`, `latest_path`, `trash_path`, `expired_date`, `isTrashRemovedPermanently`, `created_at`, `updated_at`) VALUES
(3, 'Administrator', 0, 'IT', 'IT_1589108997.zip', 'public/IT', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/IT_1589108997.zip', '2020-06-09', 0, '2020-05-10 12:09:57', '2020-05-10 12:09:57'),
(4, 'Rekam Medis & Registrasi', 1, '003-001-4-PPBT-MANAJEMEN DAN KEUANGAN.pdf', '003-001-4-PPBT-MANAJEMEN DAN KEUANGAN_1590407271.zip', 'public/Rekam Medis & Registrasi/Perpustakaan Puri Bunda Tabanan/003-001-4-PPBT-MANAJEMEN DAN KEUANGAN.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/003-001-4-PPBT-MANAJEMEN DAN KEUANGAN_1590407271.zip', '2020-06-24', 0, '2020-05-25 12:47:51', '2020-05-25 12:47:51'),
(5, 'Rekam Medis & Registrasi', 1, '003-002-4-PPBT-MANAJEMEN DAN KEUANGAN.pdf', '003-002-4-PPBT-MANAJEMEN DAN KEUANGAN_1590407280.zip', 'public/Rekam Medis & Registrasi/Perpustakaan Puri Bunda Tabanan/003-002-4-PPBT-MANAJEMEN DAN KEUANGAN.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/003-002-4-PPBT-MANAJEMEN DAN KEUANGAN_1590407280.zip', '2020-06-24', 0, '2020-05-25 12:48:00', '2020-05-25 12:48:00'),
(6, 'Rekam Medis & Registrasi', 1, '003-003-4-PPBT-MANAJEMEN DAN KEUANGAN.pdf', '003-003-4-PPBT-MANAJEMEN DAN KEUANGAN_1590407288.zip', 'public/Rekam Medis & Registrasi/Perpustakaan Puri Bunda Tabanan/003-003-4-PPBT-MANAJEMEN DAN KEUANGAN.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/003-003-4-PPBT-MANAJEMEN DAN KEUANGAN_1590407288.zip', '2020-06-24', 0, '2020-05-25 12:48:08', '2020-05-25 12:48:08'),
(7, 'Rekam Medis & Registrasi', 1, '003-009-4-PPBT-MANAJEMEN DAN KEUANGAN.pdf', '003-009-4-PPBT-MANAJEMEN DAN KEUANGAN_1590407295.zip', 'public/Rekam Medis & Registrasi/Perpustakaan Puri Bunda Tabanan/003-009-4-PPBT-MANAJEMEN DAN KEUANGAN.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/003-009-4-PPBT-MANAJEMEN DAN KEUANGAN_1590407295.zip', '2020-06-24', 0, '2020-05-25 12:48:15', '2020-05-25 12:48:15'),
(8, 'Rekam Medis & Registrasi', 1, '003-010-4-PPBT-MANAJEMEN DAN KEUANGAN.pdf', '003-010-4-PPBT-MANAJEMEN DAN KEUANGAN_1590407300.zip', 'public/Rekam Medis & Registrasi/Perpustakaan Puri Bunda Tabanan/003-010-4-PPBT-MANAJEMEN DAN KEUANGAN.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/003-010-4-PPBT-MANAJEMEN DAN KEUANGAN_1590407300.zip', '2020-06-24', 0, '2020-05-25 12:48:20', '2020-05-25 12:48:20'),
(9, 'Rekam Medis & Registrasi', 1, '003-008-4-PPBT-MANAJEMEN DAN KEUANGAN.pdf', '003-008-4-PPBT-MANAJEMEN DAN KEUANGAN_1590407304.zip', 'public/Rekam Medis & Registrasi/Perpustakaan Puri Bunda Tabanan/003-008-4-PPBT-MANAJEMEN DAN KEUANGAN.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/003-008-4-PPBT-MANAJEMEN DAN KEUANGAN_1590407304.zip', '2020-06-24', 0, '2020-05-25 12:48:24', '2020-05-25 12:48:24'),
(10, 'Rekam Medis & Registrasi', 1, '003-007-4-PPBT-MANAJEMEN DAN KEUANGAN.pdf', '003-007-4-PPBT-MANAJEMEN DAN KEUANGAN_1590407309.zip', 'public/Rekam Medis & Registrasi/Perpustakaan Puri Bunda Tabanan/003-007-4-PPBT-MANAJEMEN DAN KEUANGAN.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/003-007-4-PPBT-MANAJEMEN DAN KEUANGAN_1590407309.zip', '2020-06-24', 0, '2020-05-25 12:48:29', '2020-05-25 12:48:29'),
(11, 'Rekam Medis & Registrasi', 1, '003-006-4-PPBT-MANAJEMEN DAN KEUANGAN.pdf', '003-006-4-PPBT-MANAJEMEN DAN KEUANGAN_1590407313.zip', 'public/Rekam Medis & Registrasi/Perpustakaan Puri Bunda Tabanan/003-006-4-PPBT-MANAJEMEN DAN KEUANGAN.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/003-006-4-PPBT-MANAJEMEN DAN KEUANGAN_1590407313.zip', '2020-06-24', 0, '2020-05-25 12:48:33', '2020-05-25 12:48:33'),
(12, 'Rekam Medis & Registrasi', 1, '003-005-4-PPBT-MANAJEMEN DAN KEUANGAN.pdf', '003-005-4-PPBT-MANAJEMEN DAN KEUANGAN_1590407318.zip', 'public/Rekam Medis & Registrasi/Perpustakaan Puri Bunda Tabanan/003-005-4-PPBT-MANAJEMEN DAN KEUANGAN.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/003-005-4-PPBT-MANAJEMEN DAN KEUANGAN_1590407318.zip', '2020-06-24', 0, '2020-05-25 12:48:38', '2020-05-25 12:48:38'),
(13, 'Rekam Medis & Registrasi', 1, '003-004-4-PPBT-MANAJEMEN DAN KEUANGAN.pdf', '003-004-4-PPBT-MANAJEMEN DAN KEUANGAN_1590407323.zip', 'public/Rekam Medis & Registrasi/Perpustakaan Puri Bunda Tabanan/003-004-4-PPBT-MANAJEMEN DAN KEUANGAN.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/003-004-4-PPBT-MANAJEMEN DAN KEUANGAN_1590407323.zip', '2020-06-24', 0, '2020-05-25 12:48:43', '2020-05-25 12:48:43'),
(14, 'Sharing PKRS', 1, 'BANNER TABANAN.png', 'BANNER TABANAN_1591249134.zip', 'public/File Sharing PKRS/poster/BANNER TABANAN.png', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/BANNER TABANAN_1591249134.zip', '2020-07-04', 0, '2020-06-04 06:38:54', '2020-06-04 06:38:54'),
(15, 'Sharing PKRS', 1, 'Benner.png', 'Benner_1591250184.zip', 'public/File Sharing PKRS/Benner.png', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/Benner_1591250184.zip', '2020-07-04', 0, '2020-06-04 06:56:24', '2020-06-04 06:56:24'),
(16, 'Video List', 1, 'Perawat Ruangan Melaporkan Kondisi Pasien ke DPJP.mp4', 'Perawat Ruangan Melaporkan Kondisi Pasien ke DPJP_1594602837.zip', 'public/Video List/HPK/Perawat Ruangan Melaporkan Kondisi Pasien ke DPJP.mp4', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/Perawat Ruangan Melaporkan Kondisi Pasien ke DPJP_1594602837.zip', '2020-08-12', 0, '2020-07-13 02:13:57', '2020-07-13 02:13:57'),
(17, 'Video List', 1, 'vidio pelaporan kondisi pasien ke DPJP.mp4', 'vidio pelaporan kondisi pasien ke DPJP_1594602933.zip', 'public/Video List/HPK/vidio pelaporan kondisi pasien ke DPJP.mp4', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/vidio pelaporan kondisi pasien ke DPJP_1594602933.zip', '2020-08-12', 0, '2020-07-13 02:15:33', '2020-07-13 02:15:33'),
(18, 'Video List', 1, 'Sistem Pengolahan Air Limbah.mp4', 'Sistem Pengolahan Air Limbah_1594605176.zip', 'public/Video List/Sistem Pengolahan Air Limbah.mp4', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/Sistem Pengolahan Air Limbah_1594605176.zip', '2020-08-12', 0, '2020-07-13 02:52:57', '2020-07-13 02:52:57'),
(19, 'Video List', 1, 'Pembuangan Limbah Darah Ruang Operasi.mp4', 'Pembuangan Limbah Darah Ruang Operasi_1594608177.zip', 'public/Video List/Pembuangan Limbah Darah Ruang Operasi.mp4', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/Pembuangan Limbah Darah Ruang Operasi_1594608177.zip', '2020-08-12', 0, '2020-07-13 03:42:58', '2020-07-13 03:42:58'),
(20, 'Administrator', 1, 'Slide Dokter Update.mp4', 'Slide Dokter Update_1594619654.zip', 'public/IT/Slide Dokter Update.mp4', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/Slide Dokter Update_1594619654.zip', '2020-08-12', 0, '2020-07-13 06:54:15', '2020-07-13 06:54:15'),
(21, 'Administrator', 1, 'Video Operan Jaga.mp4', 'Video Operan Jaga_1594627080.zip', 'public/IT/Video Operan Jaga.mp4', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/Video Operan Jaga_1594627080.zip', '2020-08-12', 0, '2020-07-13 08:58:04', '2020-07-13 08:58:04'),
(22, 'Administrator', 1, 'Video Operan Jaga.mp4', 'Video Operan Jaga_1594628283.zip', 'public/IT/Video Operan Jaga.mp4', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/Video Operan Jaga_1594628283.zip', '2020-08-12', 0, '2020-07-13 09:18:07', '2020-07-13 09:18:07'),
(23, 'Video List', 0, 'VIDEO PPI', 'VIDEO PPI_1594695023.zip', 'public/Video List/VIDEO PPI', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/VIDEO PPI_1594695023.zip', '2020-08-13', 1, '2020-07-14 03:50:23', '2020-10-13 07:49:33'),
(24, 'SDM & Diklat, Sekretariat & Legal', 1, 'e-TOR Tabanan.pdf', 'e-TOR Tabanan_1598591848.zip', 'public/SDM & Diklat, Sekretariat & Legal/E-FORM SDM/e-TOR Tabanan.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/e-TOR Tabanan_1598591848.zip', '2020-09-27', 0, '2020-08-28 06:17:28', '2020-08-28 06:17:28'),
(25, 'SDM & Diklat, Sekretariat & Legal', 1, 'e-PMK Tabanan.pdf', 'e-PMK Tabanan_1598591854.zip', 'public/SDM & Diklat, Sekretariat & Legal/E-FORM SDM/e-PMK Tabanan.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/e-PMK Tabanan_1598591854.zip', '2020-09-27', 0, '2020-08-28 06:17:34', '2020-08-28 06:17:34'),
(26, 'SDM & Diklat, Sekretariat & Legal', 0, 'E-FORM SDM', 'E-FORM SDM_1598593841.zip', 'public/SDM & Diklat, Sekretariat & Legal/E-FORM SDM', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/E-FORM SDM_1598593841.zip', '2020-09-27', 0, '2020-08-28 06:50:42', '2020-08-28 06:50:42'),
(27, 'SDM & Diklat, Sekretariat & Legal', 1, 'e-PMK Tabanan.pdf', 'e-PMK Tabanan_1598597639.zip', 'public/SDM & Diklat, Sekretariat & Legal/E-FORM SDM/e-PMK Tabanan.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/e-PMK Tabanan_1598597639.zip', '2020-09-27', 0, '2020-08-28 07:53:59', '2020-08-28 07:53:59'),
(28, 'Administrator', 0, 'Modul SIMRS Medinfras', 'Modul SIMRS Medinfras_1602571718.zip', 'public/Modul SIMRS Medinfras', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/Modul SIMRS Medinfras_1602571718.zip', '2020-11-12', 0, '2020-10-13 07:48:38', '2020-10-13 07:48:38'),
(29, 'Administrator', 0, 'Modul SIMRS Medinfras', 'Modul SIMRS Medinfras_1602572904.zip', 'public/Modul SIMRS Medinfras', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/Modul SIMRS Medinfras_1602572904.zip', '2020-11-12', 0, '2020-10-13 08:08:29', '2020-10-13 08:08:29'),
(30, 'SDM & Diklat, Sekretariat & Legal', 1, 'e-Form Cuti.pdf', 'e-Form Cuti_1603415445.zip', 'public/SDM & Diklat, Sekretariat & Legal/E-FORM SDM/e-Form Cuti.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/e-Form Cuti_1603415445.zip', '2020-11-22', 0, '2020-10-23 02:10:45', '2020-10-23 02:10:45'),
(31, 'SDM & Diklat, Sekretariat & Legal', 1, 'e-TOR Tabanan.pdf', 'e-TOR Tabanan_1614908638.zip', 'public/SDM & Diklat, Sekretariat & Legal/E-FORM SDM/e-TOR Tabanan.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/e-TOR Tabanan_1614908638.zip', '2021-04-04', 0, '2021-03-05 01:43:58', '2021-03-05 01:43:58'),
(32, 'SDM & Diklat, Sekretariat & Legal', 1, 'e-PTK Tabanan.pdf', 'e-PTK Tabanan_1615953244.zip', 'public/SDM & Diklat, Sekretariat & Legal/E-FORM SDM/e-PTK Tabanan.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/e-PTK Tabanan_1615953244.zip', '2021-04-16', 0, '2021-03-17 03:54:04', '2021-03-17 03:54:04'),
(33, 'SDM & Diklat, Sekretariat & Legal', 1, 'Tutorial e-TOR Diklat.mp4', 'Tutorial e-TOR Diklat_1615953288.zip', 'public/SDM & Diklat, Sekretariat & Legal/E-FORM SDM/TUTORIAL E-FORM/Tutorial e-TOR Diklat.mp4', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/Tutorial e-TOR Diklat_1615953288.zip', '2021-04-16', 0, '2021-03-17 03:54:49', '2021-03-17 03:54:49'),
(34, 'SDM & Diklat, Sekretariat & Legal', 1, 'TUTORIAL PENGISIAN e-PMK.pdf', 'TUTORIAL PENGISIAN e-PMK_1615953295.zip', 'public/SDM & Diklat, Sekretariat & Legal/E-FORM SDM/TUTORIAL E-FORM/TUTORIAL PENGISIAN e-PMK.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/TUTORIAL PENGISIAN e-PMK_1615953295.zip', '2021-04-16', 0, '2021-03-17 03:54:55', '2021-03-17 03:54:55'),
(35, 'SDM & Diklat, Sekretariat & Legal', 1, 'TUTORIAL PENGISIAN e-PTK.pdf', 'TUTORIAL PENGISIAN e-PTK_1615953301.zip', 'public/SDM & Diklat, Sekretariat & Legal/E-FORM SDM/TUTORIAL E-FORM/TUTORIAL PENGISIAN e-PTK.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/TUTORIAL PENGISIAN e-PTK_1615953301.zip', '2021-04-16', 0, '2021-03-17 03:55:01', '2021-03-17 03:55:01'),
(36, 'SDM & Diklat, Sekretariat & Legal', 1, 'e-PMK Tabanan.pdf', 'e-PMK Tabanan_1615953651.zip', 'public/SDM & Diklat, Sekretariat & Legal/E-FORM SDM/e-PMK Tabanan.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/e-PMK Tabanan_1615953651.zip', '2021-04-16', 0, '2021-03-17 04:00:52', '2021-03-17 04:00:52'),
(37, 'SDM & Diklat, Sekretariat & Legal', 1, 'e-TOR.pdf', 'e-TOR_1615957285.zip', 'public/SDM & Diklat, Sekretariat & Legal/E-FORM SDM/e-TOR.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/e-TOR_1615957285.zip', '2021-04-16', 0, '2021-03-17 05:01:25', '2021-03-17 05:01:25'),
(38, 'SDM & Diklat, Sekretariat & Legal', 1, 'e-PMK.pdf', 'e-PMK_1615957592.zip', 'public/SDM & Diklat, Sekretariat & Legal/E-FORM SDM/e-PMK.pdf', 'C:\\xampp\\htdocs\\filemanager\\storage\\trash/e-PMK_1615957592.zip', '2021-04-16', 0, '2021-03-17 05:06:32', '2021-03-17 05:06:32'),
(39, 'IT', 1, 'README.md', 'README_1647143757.zip', 'public/IT/Item/README.md', 'C:\\SDC\\xampp\\htdocs\\uploadsip\\storage\\trash/README_1647143757.zip', '2022-04-12', 0, '2022-03-12 19:55:57', '2022-03-12 19:55:57'),
(40, 'IT', 1, 'webpack.mix.js', 'webpack.mix_1647143760.zip', 'public/IT/Item/webpack.mix.js', 'C:\\SDC\\xampp\\htdocs\\uploadsip\\storage\\trash/webpack.mix_1647143760.zip', '2022-04-12', 0, '2022-03-12 19:56:00', '2022-03-12 19:56:00'),
(41, 'IT', 1, 'webpack.mix.js', 'webpack.mix_1647143848.zip', 'public/IT/Item/webpack.mix.js', 'C:\\SDC\\xampp\\htdocs\\uploadsip\\storage\\trash/webpack.mix_1647143848.zip', '2022-04-12', 0, '2022-03-12 19:57:28', '2022-03-12 19:57:28'),
(42, 'IT', 1, 'webpack.mix.js', 'webpack.mix_1647143973.zip', 'public/IT/Item/webpack.mix.js', 'C:\\SDC\\xampp\\htdocs\\uploadsip\\storage\\trash/webpack.mix_1647143973.zip', '2022-04-12', 0, '2022-03-12 19:59:33', '2022-03-12 19:59:33');

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_folder` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kapasitas` bigint(20) NOT NULL DEFAULT 1073741824,
  `isDireksi` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `nama_unit`, `nama_folder`, `kapasitas`, `isDireksi`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'Administrator', 1073741824, 0, 1, '2020-03-13 06:56:39', '2020-03-13 06:56:39'),
(41, 'IT', 'IT', 5368709120, 0, 1, '2022-03-12 18:46:43', '2022-03-12 18:46:43'),
(42, 'Gizi', 'Gizi', 5368709120, 0, 1, '2022-03-12 20:18:11', '2022-03-12 20:18:11');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_unit` bigint(20) UNSIGNED NOT NULL,
  `id_jabatan` bigint(20) UNSIGNED NOT NULL,
  `nik` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_user` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT 0,
  `isManager` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `id_unit`, `id_jabatan`, `nik`, `nama_user`, `email`, `email_verified_at`, `password`, `isAdmin`, `isManager`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '0101001', 'Administrator', 'admin@email.com', NULL, '$2a$12$1AzIOYa.CmugCSH5meiho.7rLKEZSjcPrpUldul74.4lr27F0Y1BK', 1, 0, 1, NULL, '2020-03-13 06:56:39', '2020-03-13 06:56:39'),
(57, 41, 2, '020202', 'it', 'testing@test.com', NULL, '$2y$10$1m9iIlROznufHm5aZyZfF.iGlsXAqxL.HQ3l2aSstQ0iu5X8plibC', 0, 1, 1, NULL, '2022-03-12 18:51:49', '2022-03-12 18:51:49'),
(58, 42, 2, '030303', 'gizi', 'testing@test.com', NULL, '$2y$10$ZE4cX9wk71thnbTTnNN6EuOWuW/NepEwTRjROnz4wtb18ZrynKfwy', 0, 0, 1, NULL, '2022-03-12 20:18:40', '2022-03-12 20:18:40');

-- --------------------------------------------------------

--
-- Table structure for table `user_logs`
--

CREATE TABLE `user_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `id_file` bigint(20) UNSIGNED DEFAULT NULL,
  `id_folder` bigint(20) UNSIGNED DEFAULT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aksi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_logs`
--

INSERT INTO `user_logs` (`id`, `id_user`, `id_file`, `id_folder`, `path`, `aksi`, `created_at`, `updated_at`) VALUES
(1, 57, NULL, 2, 'public/IT/Item', 'it membuat folder Item', '2022-03-12 18:55:40', '2022-03-12 18:55:40'),
(2, 57, 1, NULL, 'public/IT/Item/README.md', 'it meng-upload file README.md', '2022-03-12 19:16:57', '2022-03-12 19:16:57'),
(3, 57, 2, NULL, 'public/IT/Item/webpack.mix.js', 'it meng-upload file webpack.mix.js', '2022-03-12 19:52:17', '2022-03-12 19:52:17'),
(4, 57, 1, NULL, 'public/IT/Item/README.md', 'it menghapus file README.md', '2022-03-12 19:55:57', '2022-03-12 19:55:57'),
(5, 57, 2, NULL, 'public/IT/Item/webpack.mix.js', 'it menghapus file webpack.mix.js', '2022-03-12 19:56:00', '2022-03-12 19:56:00'),
(6, 57, 3, NULL, 'public/IT/Item/webpack.mix.js', 'it meng-upload file webpack.mix.js', '2022-03-12 19:56:17', '2022-03-12 19:56:17'),
(7, 57, 3, NULL, 'public/IT/Item/webpack.mix.js', 'it menghapus file webpack.mix.js', '2022-03-12 19:57:28', '2022-03-12 19:57:28'),
(8, 57, 4, NULL, 'public/IT/Item/webpack.mix.js', 'it meng-upload file webpack.mix.js', '2022-03-12 19:58:16', '2022-03-12 19:58:16'),
(9, 57, 4, NULL, 'public/IT/Item/webpack.mix.js', 'it menghapus file webpack.mix.js', '2022-03-12 19:59:33', '2022-03-12 19:59:33'),
(10, 57, 5, NULL, 'public/IT/Item/webpack.mix.js', 'it meng-upload file webpack.mix.js', '2022-03-12 20:04:07', '2022-03-12 20:04:07'),
(11, 58, 6, NULL, 'public/Gizi/server.php', 'gizi meng-upload file server.php', '2022-03-12 20:33:23', '2022-03-12 20:33:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `file_meta_data`
--
ALTER TABLE `file_meta_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `file_meta_data_id_unit_foreign` (`id_unit`);

--
-- Indexes for table `folders`
--
ALTER TABLE `folders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `folders_id_unit_foreign` (`id_unit`);

--
-- Indexes for table `jabatans`
--
ALTER TABLE `jabatans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_id_user_foreign` (`id_user`);

--
-- Indexes for table `trashes`
--
ALTER TABLE `trashes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_id_unit_foreign` (`id_unit`),
  ADD KEY `users_id_jabatan_foreign` (`id_jabatan`);

--
-- Indexes for table `user_logs`
--
ALTER TABLE `user_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_logs_id_user_foreign` (`id_user`),
  ADD KEY `user_logs_id_file_foreign` (`id_file`),
  ADD KEY `user_logs_id_folder_foreign` (`id_folder`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `file_meta_data`
--
ALTER TABLE `file_meta_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `folders`
--
ALTER TABLE `folders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jabatans`
--
ALTER TABLE `jabatans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `trashes`
--
ALTER TABLE `trashes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `user_logs`
--
ALTER TABLE `user_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `file_meta_data`
--
ALTER TABLE `file_meta_data`
  ADD CONSTRAINT `file_meta_data_id_unit_foreign` FOREIGN KEY (`id_unit`) REFERENCES `units` (`id`);

--
-- Constraints for table `folders`
--
ALTER TABLE `folders`
  ADD CONSTRAINT `folders_id_unit_foreign` FOREIGN KEY (`id_unit`) REFERENCES `units` (`id`);

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_id_jabatan_foreign` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatans` (`id`),
  ADD CONSTRAINT `users_id_unit_foreign` FOREIGN KEY (`id_unit`) REFERENCES `units` (`id`);

--
-- Constraints for table `user_logs`
--
ALTER TABLE `user_logs`
  ADD CONSTRAINT `user_logs_id_file_foreign` FOREIGN KEY (`id_file`) REFERENCES `file_meta_data` (`id`),
  ADD CONSTRAINT `user_logs_id_folder_foreign` FOREIGN KEY (`id_folder`) REFERENCES `folders` (`id`),
  ADD CONSTRAINT `user_logs_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
