<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Folder;
use Illuminate\Support\Facades\Auth;
use App\FileMetaData;
use App\Unit;
use \Madnest\Madzipper\Madzipper;
use App\Trash;

// Cek konten apakah folder atau file
function checkType($path)
{
    if (is_dir(storage_path('app/'.$path))) {
        return 'folder';
    }else {
        return 'file';
    }
};

// Hitung jumlah folder dalam direktori
function countFolder($path)
{
    if (Auth::user()->isAdmin || Auth::user()->unit->isDireksi) {
        return count(Storage::directories($path));
    } else {
        $privateFolder = Folder::where(['isPrivate' => 1, 'status' => 1])->where('id_unit', '!=', Auth::user()->id_unit)->pluck('path')->toArray();
        return count(array_diff(Storage::directories($path), $privateFolder));
    }
}

// Hitung jumlah file dalam direktori
function countFile($path)
{
    if (Auth::user()->isAdmin || Auth::user()->unit->isDireksi) {
        return count(Storage::files($path));
    } else {
        $privateFile = FileMetaData::where(['isPrivate' => 1, 'status' => 1])->where('id_unit', '!=', Auth::user()->id_unit)->pluck('path')->toArray();
        return count(array_diff(Storage::files($path), $privateFile));
    }
}

//
function getContentByPath($path)
{
    $exploded = explode('/', $path);

    return Str::after($path, $exploded[0]);
}

// Menampilkan nama file/folder berdasarkan path
function contentName($path)
{
    $exploded = explode('/', $path);
    $num = count($exploded);
    $i = 0;
    foreach ($exploded as $e) {
        if (++$i === $num) {
            return $e;
        }
    }
}

function relativePath($path)
{
    $exploded = explode('/', $path);
    $rel = '';
    for ($i=0; $i < count($exploded) ; $i++) {
        if ($i != 0) {
            $rel = $rel.' / '.$exploded[$i];
        }
    }

    return $rel;
}

// Mencari parent path dari folder atau file
function parentPath($currentPath)
{
    $exploded = explode('/', $currentPath);
    $num = count($exploded);
    $i = 0;
    $last = '';
    foreach ($exploded as $e) {
        if (++$i === $num) {
            $last = '/'.$e;
        }
    }

    return Str::before($currentPath, $last);
}

// Path file/folder untuk breadcrumb
function breadcrumbPath($currentPath, $name)
{
    return Str::before($currentPath, $name).$name;
}

// Menampilkan ekstensi dari sebuah file
function getExtension($path)
{
    return pathinfo($path, PATHINFO_EXTENSION);
}

// Menampilkan nama dari sebuah file
function nameOnly($path)
{
    return pathinfo($path, PATHINFO_FILENAME);
}

// Menampilkan tanggal dan waktu dari
function modified($path)
{
    return Carbon::createFromTimestamp(Storage::lastModified($path))->toDateTimeString();
}

// Cek apakah path yang diakses merupakan root path milik unit yang login
function rootUnit($currentPath)
{
    if (Auth::user()->isAdmin) {
        return true;
    }

    $root = Folder::where(['id_unit' => Auth::user()->id_unit, 'root' => 1, 'status' => 1])->value('path');
    return Str::contains($currentPath, $root);
}

// Untuk mengambil path file yang memiliki status private
function priv($paths)
{
    if (Auth::user()->isAdmin || Auth::user()->unit->isDireksi) {
        return $paths;
    } else {
        $privateFolder = Folder::where(['isPrivate' => 1, 'status' => 1])->where('id_unit', '!=', Auth::user()->id_unit)->pluck('path')->toArray();
        $privateFile = FileMetaData::where(['isPrivate' => 1, 'status' => 1])->where('id_unit', '!=', Auth::user()->id_unit)->pluck('path')->toArray();

        $private = array_merge($privateFolder, $privateFile);
        return array_diff($paths, $private);
    }
}

// Konversi tanggal dan waktu dari UNIX timestamp menjadi DateTimeString
function localDateTimeFormat($date)
{
    return Carbon::parse($date)->toDateTimeString();
}

// Cek status file/folder private atau shared
function contentStatus($path, $data)
{
    foreach ($data as $d) {
        if ($path == $d->path) {
            return $d->isPrivate == 1 ? 'Private' : 'Shared';
        }
    }
}

// Cek unit pemilik file/folder dari sebuah path
function whoHasThisPath($path)
{
    if (checkType($path) == 'folder') {
        return Folder::where(['path' => $path, 'status' => 1])->value('id_unit');
    } else {
        return FileMetaData::where(['path' => $path, 'status' => 1])->value('id_unit');
    }
}

// Cek apakah action button dapat di render atau tidak.
// Action button dapat di render apabila path yang diakses sesuai dengan path yang dimiliki oleh user yang login
function renderActionButton($path)
{
    if (Auth::user()->isAdmin) {
        return 1;
    }

    return whoHasThisPath($path) == Auth::user()->id_unit ? 1 : 0;
}

function cannotAccess()
{
    $user = Auth::user()->permission;

    if ($user->view || $user->create || $user->update || $user->delete || $user->download) {
        return false;
    } else {
        return true;
    }
}

function moveToTrash($pathToTrash)
{
    $unitId = whoHasThisPath($pathToTrash);
    $namaUnit = Unit::where(['id' => $unitId])->value('nama_unit');

    $nama = nameOnly($pathToTrash);
    $zipName = $nama.'_'.now()->timestamp;
    $zipPath = storage_path('trash').'/'.$zipName.'.zip';
    $ToZip = storage_path('app/'.$pathToTrash);
    // $zipPathInStorage = '/'.'trash/'.$zipName.'.zip';
    // dd($zipPathInStorage);

    Madzipper::make($zipPath)->add($ToZip)->close();

    Trash::create([
        'nama_unit' => $namaUnit,
        'isFile' => checkType($pathToTrash) == 'file' ? 1 : 0,
        'nama_asli' => contentName($pathToTrash),
        'nama_trash' => $zipName.'.zip',
        'latest_path' => $pathToTrash,
        'trash_path' => $zipPath,
        'expired_date' => Carbon::now()->addDays(30)->toDateString()
    ]);

    return true;
}

function isExpired($expdate)
{
    $exp = Carbon::parse($expdate);
    if (Carbon::now()->greaterThanOrEqualTo($exp)) {
        return true;
    } else {
        return false;
    }
}

// Mengambil semua data folder yang aktif
function foldersData()
{
    return Folder::where('status', 1)->get();
}

// Mengambil semua data file yang aktif
function filesData()
{
    return FileMetaData::where('status', 1)->get();
}

// Konversi dari Gigabyte ke Byte
function fromGigaToBytes($value)
{
    return $value * pow(1024, 3);
}

// Konversi dari Byte ke Gigabyte
function fromBytesToGiga($value)
{
    return $value / pow(1024, 3);
}

// Mengambil size dari sebuah file
function fileSize2($path)
{
    return formatSizeUnits(Storage::size($path));
}

// Mengambil total size dari sebuah folder
function folderSize($path)
{
    $size = 0;
    foreach (Storage::allFiles($path) as $file) {
        $size += Storage::size($file);
    }

    return formatSizeUnits($size);
}

// Mengambil kapasitas tersedia dari kapasitas maksimum yang dimiliki oleh unit
function diskSpace()
{
    $root = Folder::where(['id_unit' => Auth::user()->id_unit, 'root' => 1])->value('path');
    $capacity = Unit::where(['id' => Auth::user()->id_unit])->value('kapasitas');

    return 'Kapasitas : '.folderSize($root).' digunakan dari '.formatSizeUnits($capacity).' tersedia';
}

// Mengambil kapasitas tersedia dari kapasitas maksimum yang dimiliki oleh unit untuk administrator
function diskSpaceForAdmin($currentPath)
{
    $unit = whoHasThisPath($currentPath);

    $root = Folder::where(['id_unit' => $unit, 'root' => 1])->value('path');
    $capacity = Unit::where(['id' => $unit])->value('kapasitas');

    return 'Kapasitas : '.folderSize($root).' digunakan dari '.formatSizeUnits($capacity).' tersedia';
}

// Mengambil jumlah kapasitas yang sudah digunakan oleh unit
function usage()
{
    $root = Folder::where(['id_unit' => Auth::user()->id_unit, 'root' => 1])->value('path');
    $usage = 0;
    foreach (Storage::allFiles($root) as $file) {
        $usage += Storage::size($file);
    }

    return $usage;
}

// Mengambil kapasitas yang dimiliki oleh unit
function capacity()
{
    return Unit::where(['id' => Auth::user()->id_unit])->value('kapasitas');
}

// Memeriksa ketersediaan kapasitas apakah masih memungkinkan untuk meng-upload file atau tidak
function capacityIsNotFull($size)
{
    $usage = usage();
    $capacity = capacity();
    $estimatedSize = $usage + $size;

    return $estimatedSize < $capacity ? true : false;
}

// Menentukan presentase kapasitas tersedia (digunakan untuk tampilan bar)
function capacityWidth()
{
    $usage = usage();
    $capacity = capacity();

    if ($usage != 0) {
        return ($usage / $capacity) * 100;
    } else {
        return 0;
    }
}

// Menentukan presentase kapasitas tersedia (digunakan untuk tampilan bar untuk administrator)
function capacityWidthForAdmin($currentPath)
{
    if ($currentPath != 'public') {
        $unit = whoHasThisPath($currentPath);

        $root = Folder::where(['id_unit' => $unit, 'root' => 1])->value('path');
        $usage = 0;
        foreach (Storage::allFiles($root) as $file) {
            $usage += Storage::size($file);
        }

        $capacity = Unit::where(['id' => $unit])->value('kapasitas');

        if ($usage != 0) {
            return ($usage / $capacity) * 100;
        } else {
            return 0;
        }
    }
}

// Menentukan warna yang digunakan sesuai dengan jumlah kapasitas yang sudah digunakan (digunakan untuk tampilan bar)
function capacityColor($width)
{
    if ($width <= 40) {
        return 'bg-success';
    } elseif ($width > 40 && $width <= 65){
        return 'bg-primary';
    } elseif ($width > 65 && $width <= 84) {
        return 'bg-warning';
    } else {
        return 'bg-danger';
    }

}

// Memberikan format penulisan untuk size (GB, MB, KB, bytes, byte)
function formatSizeUnits($bytes)
{
    if ($bytes >= 1073741824)
    {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    }
    elseif ($bytes >= 1024)
    {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    }
    elseif ($bytes > 1)
    {
        $bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1)
    {
        $bytes = $bytes . ' byte';
    }
    else
    {
        $bytes = '0 bytes';
    }

    return $bytes;
}
