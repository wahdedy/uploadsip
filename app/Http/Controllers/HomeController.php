<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FileMetaData;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $date = today()->format('Y-m-d');
        $sharedfile = FileMetaData::where("expDate","<",$date)
        ->where('id_unit', '=', Auth::user()->id_unit)
        ->orWhere(function ($query) {
            $query->where('id_unit', '!=', Auth::user()->id_unit)
                  ->where('isPrivate', '=', 0);
        })->get()
        ;

        foreach($sharedfile as $sf){
            toastr()->error('File '. $sf->nama_file .' sudah kadaluarsa ('.$sf->expDate.')', 'Error');
        }

        return view('home');
    }
}
